package main

import (
	"./boolfunc"
	"fmt"
)

func main() {

	numInputs := uint32(3)

	fmt.Printf("Performing analysis with %d inputs\n", numInputs)
	report := boolfunc.AnalyzeEV3(numInputs)
	fmt.Printf("Negative: %d, Zero: %d, Positive: %d\n",
		report.Counts.Negative, report.Counts.Zero, report.Counts.Positive)
	fmt.Printf("Correlated funcs: %d\n", len(report.CorrelatedFuncs))

}
