package boolfunc

import (
	"fmt"
	"github.com/golang-collections/go-datastructures/bitarray"
	"math/rand"
	"runtime"
	"strings"
	"sync"
	"time"
)

// Random number generator for this package
var pkgRand *rand.Rand

// Init the package
func init() {
	// Seed the random number generator with the current time
	// so every execution is different.
	pkgRand = rand.New(rand.NewSource(time.Now().Unix()))
}

// Make all increasing boolean functions that take the given number of inputs.
func MakeAllIncr(numInputs uint32) []BoolFunc {

	// Base case - 0 inputs
	if numInputs == 0 {

		zero, one := bitarray.NewBitArray(1), bitarray.NewBitArray(1)
		one.SetBit(0)

		return []BoolFunc{
			{0, zero},
			{0, one},
		}
	}

	// Recursive case - get all bit arrays from MakeAllIncr(N-1).
	// Double the width of each one, such that the original bits are on the right (lower-order bits).
	// Fill in the empty higher order bits with every bit-array same or larger.
	// Keep it if it's an increasing function.

	previous := MakeAllIncr(numInputs - 1)
	var current []BoolFunc

	for i := 0; i < len(previous); i++ {

		for j := i; j < len(previous); j++ {

			// Reference to lower bits we are starting with
			lowerBits := &previous[i].truthTable

			// Reference to higher bits we might add
			higherBits := &previous[j].truthTable

			// New truth table is twice as wide as the old one.
			candidateTable := bitarray.NewBitArray(powerOf2(numInputs))

			// Copy lower bits into the bottom half.
			for inputCombo := uint64(0); inputCombo < powerOf2(numInputs-1); inputCombo++ {
				shouldSetLowerBit, _ := (*lowerBits).GetBit(inputCombo)
				if shouldSetLowerBit {
					candidateTable.SetBit(inputCombo)
				}
			}

			// Copy higher bits into the upper half.
			for inputCombo := uint64(0); inputCombo < powerOf2(numInputs-1); inputCombo++ {
				shouldSetHigherBit, _ := (*higherBits).GetBit(inputCombo)
				if shouldSetHigherBit {
					candidateTable.SetBit(powerOf2(numInputs-1) + inputCombo)
				}
			}

			// If candidate is an increasing boolean function,
			// it is successful and can be added to the slice for return.
			candidateFunc := BoolFunc{numInputs, candidateTable}
			if isIncreasing(&candidateFunc) {
				current = append(current, candidateFunc)
			}
		}
	}

	return current
}

// Create a random increasing boolean function that takes the given number of inputs,
// and the given true-probability configuration.
func MakeRandomIncr(numInputs uint32, trueProb TrueProbability) BoolFunc {

	// Create boolean function struct.
	bf := BoolFunc{numInputs, bitarray.NewBitArray(powerOf2(numInputs))}

	// Randomly set each value to 0 or 1.
	for inputCombo := uint64(0); inputCombo < powerOf2(numInputs); inputCombo++ {

		// Check if we are required to set this bit to true.
		// This will be the case if any earlier combos with the same active input cells
		// were set to 1.
		if positionMustBeTrue(&bf, inputCombo) {
			bf.truthTable.SetBit(inputCombo)
			continue
		}

		// Otherwise, if we weren't forced to, set the bit to true based
		// on the probability specified.
		randFloat := pkgRand.Float64()
		currentTrueProb := getTrueProb(inputCombo, powerOf2(numInputs), &trueProb)
		if randFloat >= (1 - currentTrueProb) {
			bf.truthTable.SetBit(inputCombo)
		}
	}

	return bf
}

// Print out the boolean function as a string - each possible input and its resulting output.
func ToString(bf BoolFunc) string {

	var builder strings.Builder

	if bf.numInputs == 0 {
		bit, _ := bf.truthTable.GetBit(0)
		fmt.Fprintf(&builder, "[%t]", bit)
		return builder.String()
	}

	for inputCombo := uint64(0); inputCombo < powerOf2(bf.numInputs); inputCombo++ {

		if inputCombo > 0 {
			fmt.Fprint(&builder, ", ")
		}

		fmt.Fprint(&builder, "[")
		for inputCell := bf.numInputs - 1; inputCell >= 0; {

			fmt.Fprintf(&builder, "%d", (inputCombo&(1<<inputCell))>>inputCell)

			if inputCell == 0 {
				break
			} else {
				fmt.Fprint(&builder, " ")
			}
			inputCell--
		}

		bit, _ := bf.truthTable.GetBit(inputCombo)
		fmt.Fprintf(&builder, ": %t]", bit)
	}

	return builder.String()
}

// Do an analysis of expected values over all increasing functions of N inputs, 3 at a time.
// Uses all cores available on the machine!
func AnalyzeEV3(numInputs uint32) EV3Report {

	bfs := MakeAllIncr(numInputs)

	var fullEV3Report EV3Report

	// Save number of cores available.
	numCores := runtime.NumCPU()

	for i := 0; i < len(bfs); i++ {
		for j := i; j < len(bfs); j++ {

			kRange := len(bfs) - j

			// We will block while we are waiting for all goroutines to finish.
			var waitGroup sync.WaitGroup
			waitGroup.Add(numCores)

			// Make a channel with EV counts to receive results from each goroutine.
			partialEV3ReportChannel := make(chan EV3Report, numCores)

			// Dispatch an even amount of work to each goroutine.
			for core := 0; core < numCores; core++ {
				kMin := j + (core*kRange)/numCores
				kMax := j + ((core+1)*kRange)/numCores

				go analyzeEV3Partial(&bfs, i, j, kMin, kMax, partialEV3ReportChannel, &waitGroup)
			}

			// Block while we wait for all goroutines to finish executing.
			waitGroup.Wait()
			close(partialEV3ReportChannel)

			// Once all done, add partial reports to the full one.
			for partialEV3Report := range partialEV3ReportChannel {
				fullEV3Report.add(&partialEV3Report)
			}
		}
	}

	return fullEV3Report
}

// Helper for AnalyzeEV3 that iterates over a partial range of k (the third helper function).
func analyzeEV3Partial(bfs *[]BoolFunc, i int, j int, kMin int, kMax int, c chan EV3Report, wg *sync.WaitGroup) {

	defer wg.Done()

	var ev3Report EV3Report

	for k := kMin; k < kMax; k++ {

		bf1, bf2, bf3 := &(*bfs)[i], &(*bfs)[j], &(*bfs)[k]

		ev := ev3(bf1, bf2, bf3)

		if floatIsZero(ev) {
			ev3Report.Counts.Zero++
			if are3Correlated(bf1, bf2, bf3) {
				ev3Report.CorrelatedFuncs = append(ev3Report.CorrelatedFuncs,
					FuncTriple{Bf1: *bf1, Bf2: *bf2, Bf3: *bf3})
			}
		} else if ev < 0 {
			ev3Report.Counts.Negative++
		} else {
			ev3Report.Counts.Positive++
		}
	}

	c <- ev3Report
}
