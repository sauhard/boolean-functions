package boolfunc

// Return whether the function specified is an increasing function.
func isIncreasing(bf *BoolFunc) bool {

	isIncreasing := true
	for inputCombo := uint64(1); inputCombo < powerOf2(bf.numInputs); inputCombo++ {
		if positionMustBeTrue(bf, inputCombo) {
			isIncreasing, _ = bf.truthTable.GetBit(inputCombo)
			if !isIncreasing {
				break
			}
		}
	}

	return isIncreasing
}

// Return whether the position specified must be true, based on previous inputs
// for the function to be an increasing boolean function.
func positionMustBeTrue(bf *BoolFunc, position uint64) bool {
	forced := false
	for inputCell := uint32(0); inputCell < bf.numInputs && !forced; inputCell++ {
		earlierPosition := position & ^(1 << inputCell)
		isSet, _ := bf.truthTable.GetBit(earlierPosition)
		if isSet {
			forced = true
		}
	}

	return forced
}

// Return whether the three functions provided are correlated.
// They must all have the same number of inputs. It is the caller's responsibility to ensure this.
func are3Correlated(bf1 *BoolFunc, bf2 *BoolFunc, bf3 *BoolFunc) bool {
	return are2Correlated(bf1, bf2) && are2Correlated(bf2, bf3) && are2Correlated(bf1, bf3)
}

// Return whether the two functions provided are correlated.
// They must all have the same number of inputs. It is the caller's responsibility to ensure this.
func are2Correlated(bf1 *BoolFunc, bf2 *BoolFunc) bool {
	return ev(bf1, bf2) > (ev(bf1) * ev(bf2))
}

// Return the expected value of three boolean functions.
// They must all have the same number of inputs. It is the caller's responsibility to ensure this.
func ev3(bf1 *BoolFunc, bf2 *BoolFunc, bf3 *BoolFunc) float64 {

	return 2*ev(bf1, bf2, bf3) + ev(bf1)*ev(bf2)*ev(bf3) -
		ev(bf1)*ev(bf2, bf3) - ev(bf2)*ev(bf1, bf3) - ev(bf3)*ev(bf1, bf2)
}

// Return the expected value of a boolean function consisting of all the ones passed in AND'ed together.
// They must all have the same number of inputs. It is the caller's responsibility to ensure this.
func ev(bfs ...*BoolFunc) float64 {

	var sum int64

	for inputCombo := uint64(0); inputCombo < powerOf2(bfs[0].numInputs); inputCombo++ {

		allYes := true

		for _, bf := range bfs {
			bit, _ := bf.truthTable.GetBit(inputCombo)
			if !bit {
				allYes = false
				break
			}
		}

		if allYes {
			sum++
		}
	}

	return float64(sum) / float64(powerOf2(bfs[0].numInputs))
}
