package boolfunc

import "github.com/golang-collections/go-datastructures/bitarray"

// A boolean function has a specified number of inputs, and a
// specific true/false output value for each combination of inputs.
type BoolFunc struct {

	// How many inputs we are passing in
	numInputs uint32

	// The truth table of outputs
	truthTable bitarray.BitArray
}

// Used to help build a boolean function, this struct contains probabilities
// that we use to decide whether a given output should be true.
type TrueProbability struct {

	// Likelihood for true to be picked at the first cell (all 0's)
	BeforeWarmup float64

	// Likelihood for true to be picked after warmup is complete
	AfterWarmup float64

	// Percentage of overall combinations that the warmup takes
	WarmupSetRatio float64
}

// Analysis result from studying expected values on a combination of boolean functions
type EVCounts struct {

	// How many times we see a negative EV
	Negative uint64

	// How many times we see a zero EV
	Zero uint64

	// How many times we see a positive EV
	Positive uint64
}

// Add another EVCounts to this one.
func (thisEvc *EVCounts) add(otherEvc *EVCounts) {
	thisEvc.Negative += otherEvc.Negative
	thisEvc.Zero += otherEvc.Zero
	thisEvc.Positive += otherEvc.Positive
}

// A set of three boolean functions
type FuncTriple struct {

	// First function
	Bf1 BoolFunc

	// Second function
	Bf2 BoolFunc

	// Third function
	Bf3 BoolFunc
}

// A report EV  when doing an expected value calculation on
// 3 boolean functions at a time
type EV3Report struct {

	// A list of counts
	Counts EVCounts

	// Function triples that are correlated (i.e., with an EV of 0)
	CorrelatedFuncs []FuncTriple
}

// Add another EV3Report to this one.
func (thisReport *EV3Report) add(otherReport *EV3Report) {
	thisReport.Counts.add(&otherReport.Counts)
	thisReport.CorrelatedFuncs = append(thisReport.CorrelatedFuncs, otherReport.CorrelatedFuncs...)
}
