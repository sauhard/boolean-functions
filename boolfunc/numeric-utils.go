package boolfunc

import "math"

// Determine if a float value is effectively zero.
func floatIsZero(x float64) bool {
	const epsilon = 1e-9
	return math.Abs(x) < epsilon
}

// Return 2^n
func powerOf2(n uint32) uint64 {

	// Return pre-computed answer for easy cases
	switch n {
	case 0:
		return 1
	case 1:
		return 2
	case 2:
		return 4
	case 3:
		return 8
	case 4:
		return 16
	case 5:
		return 32
	case 6:
		return 64
	case 7:
		return 128
	case 8:
		return 256
	}

	// Simple loop to calculate N=9 or higher
	result := uint64(512)
	for i := uint32(9); i < n; i++ {
		result = result * 2
	}
	return result
}

// Get appropriate probability to use, given current combo number, all combos, and
// the probability object.
func getTrueProb(currentCombo uint64, totalCombos uint64, trueProb *TrueProbability) float64 {

	warmupDoneCombo := uint64(float64(totalCombos) * trueProb.WarmupSetRatio)

	if currentCombo > warmupDoneCombo {
		return trueProb.AfterWarmup
	}

	prob := trueProb.BeforeWarmup +
		(trueProb.AfterWarmup-trueProb.BeforeWarmup)*
			float64(currentCombo)/float64(warmupDoneCombo)

	// In case the warmup size was really small, it's possible we did a divide by zero.
	// Assume zero in this case.
	if math.IsNaN(prob) {
		prob = float64(0)
	}

	return prob
}
